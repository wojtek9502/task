from app.models import User
from django.core.management.base import BaseCommand


def is_product_in_user_transactions(user_queryset, search_product_name):
    user_transations = user_queryset.payment_transactions.all()
    for user_transaction in user_transations:
        if user_transaction.product_name == search_product_name:
            return True
    return False


class Command(BaseCommand):
    help = 'This script search all users transactions. If user have any transaction with product_name=="Free Trial",' \
           ' script will change this user status to "free".'
    product_name = "Free Trial"

    def cmd_print(self, text):
        self.stdout.write(text)

    def handle(self, *args, **options):
        users = User.objects.prefetch_related("payment_transactions").all()
        for user in users:
            self.cmd_print(f'user: {user.first_name} status: "{user.status}"')

            new_user_status = "premium"
            if is_product_in_user_transactions(user, search_product_name=self.product_name):
                self.cmd_print(f'Found product: "{self.product_name}" in transactions for user: "{user.first_name}"')
                new_user_status = "free"

            if user.status != new_user_status:
                user.status = new_user_status
                user.save()
                self.cmd_print(f'User: "{user.first_name}" status set to "{user.status}"')
