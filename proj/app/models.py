from django.db import models

# Create your models here.
class TimeStampedModel():
    pass


class User(models.Model, TimeStampedModel):
    COUNTRIES_CHOICES = [
        ("PL", "pl")
    ]

    first_name = models.CharField(max_length=75, verbose_name='first name', blank=True)
    last_name = models.CharField(max_length=75, verbose_name='last name', blank=True)
    phone_number = models.CharField(max_length=50, verbose_name='phone number', blank=True)
    country = models.CharField(max_length=2, choices=COUNTRIES_CHOICES, verbose_name='country', blank=True)
    state = models.CharField(max_length=75, verbose_name='state', blank=True)
    city = models.CharField(max_length=100, verbose_name='city', blank=True)
    zip_code = models.CharField(max_length=15, verbose_name='ZIP code', blank=True)
    street = models.CharField(max_length=100, verbose_name='street', blank=True)
    vat_number = models.CharField(verbose_name='VAT ID', max_length=15, blank=True, db_index=True)
    active = models.BooleanField(default=False, verbose_name="Is user active?")
    status = models.CharField(max_length=10, blank=False, default='free')

    def __str__(self):
        return f"user: {self.first_name} status: {self.status}"

class TransactionHistory(models.Model, TimeStampedModel):
    PAYMENT_STATUS = [
        ('SUCCESS', 'Success'),
        ('PENDING', 'Pending'),
        ('ERROR', 'Error')
    ]

    TRANSACTION_TYPE = [
        ('SALE', 'Sale'),
        ('GIFT', 'Gift'),
        ('RENEWAL', 'Renewal'),
        ('TRIAL', 'Trial'),
        ('OTHER', 'Other')
    ]

    billing_date = models.DateTimeField(auto_now_add=True, blank=False)
    user = models.ForeignKey(User, verbose_name='user', on_delete=models.SET_NULL, related_name='payment_transactions',
                             null=True, default=None, blank=False)
    transaction_type = models.CharField(verbose_name='Transaction type', choices=TRANSACTION_TYPE,
                                        default='SALE', max_length=10, db_index=True)
    transaction_date = models.DateTimeField(verbose_name='receipt date', auto_now_add=True)
    payment_status = models.CharField(verbose_name='payment status', max_length=10, db_index=True,
                                      choices=PAYMENT_STATUS, default='SUCCESS')
    product_name = models.CharField("product name", max_length=255, unique=True)

    def __str__(self):
        return f"user: {self.user.first_name} product: {self.product_name}"