### Requirements
- Python 3.8

### Install

- install requirements
```sh
python -m pip install -r requirements.txt  
```
- Add 'app' to INSTALLED_APP in proj/settings.py

- migrate
```sh
cd proj
python manage.py makemigrations
python manage.py migrate
```
- create superuser for admin panel
```sh
cd proj
python manage.py createsuperuser
```

### Run command
```shell
cd proj
python manage.py update_users_status
```

### Test


- In admin panel, you need to create User with status premium, and create TransactionHistory with product_name="Free Trial" linked to this user.
- Run command
- Delete TransactionHistory obj in admin panel
- Run command